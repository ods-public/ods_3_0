# Как использовать pre-commit

- Чтобы использовать pre-commit в локальном репозитории, необходимо установить в локальном окружение `pre-commit`:
```
pip3 install pre-commit
```
- после этого инициализировать pre-commit конфиг с помощью, конфигурация подтянется из `.pre-commit-config.yaml`:
```
pre-commit install
```

Если не настроены всякие плагины в IDE, которые автоформатируют содержимое файлов под требование, то при выполнение:
```
git commit -m '<some-text>'
```

могут выявится ошибки в форматировании содержимого или содержимого самих файлов, которые будет необходимо исправить, например

```
git commit -m "test"
trim trailing whitespace.................................................Passed
fix end of files.........................................................Passed
check for added large files..............................................Passed
check for merge conflicts................................................Passed
flake8...................................................................Failed
- hook id: flake8
- exit code: 1

mypy.py:1:31: F821 undefined name 'inadf'

ruff.....................................................................Failed
- hook id: ruff
- exit code: 1

mypy.py:1:31: F821 Undefined name `inadf`
Found 1 error.

mypy.....................................................................Failed
- hook id: mypy
- exit code: 1

mypy.py:1: error: Name "inadf" is not defined  [name-defined]
Found 1 error in 1 file (checked 1 source file)
```
